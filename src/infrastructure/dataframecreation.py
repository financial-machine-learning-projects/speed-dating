import os
from os import path
from os.path import join
import sys

import pandas as pd
import random

import src.settings.base as stg

class DataFrameBuilder:
    
    def __init__(self, filename):
        
        """Initialize Class

        Parameters
        ----------
        filename : string
            filename of dataset (must be csv for now)
        """
        
        self.filename = filename


    @property
    def data(self):
        
        if self.filename.endswith(".xlsx"):
            df = pd.read_excel(join(stg.DATA_DIR, self.filename))
            
            return df
        
        if self.filename.endswith(".csv"):
            df = pd.read_csv(join(stg.DATA_DIR, self.filename), encoding="ISO-8859-1")
            
            return df
                    
        else:
            raise FileExistsError("Extension must be xlsx or csv.")
            

def random_date_data():
    
    df = pd.DataFrame(
    {"attr_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)],
     "sinc_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)],
     "intel_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)],
     "fun_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)],
     "amb_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)],
     "shar_o": [float(random.randint(0.0, 10.0)) for x in range(0,10)]}, 
    index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
     )
    
    return df