import streamlit as st

import os
from os import path
from os.path import join
import sys

import pandas as pd

import src.settings.base as stg
from src.infrastructure.dataframecreation import DataFrameBuilder
from src.domain.featureselection import FeatureSelector
from src.application.decision_model import Speed_Dating_Prediction

from sklearn.model_selection import train_test_split


def project_description():
    """Streamlit widget to show the project description on the home page."""
    # Text/Title
    st.title("Prédiction des décisions romantiques féminines")
    # Image
    st.image("https://cache.cosmopolitan.fr/data/photo/w2000_ci/57/bon-baiser-de-bruges.jpg",
             width=700,
             caption=" ")
    # Header/Subheader
    st.header("Par **Damien Mellot**")
    st.header("Sopra Steria, **Projet Speed Dating**, juin 2021")
    st.subheader("")
    st.header("Évaluez-vous objectivement sur six qualités fondamentales que toute femme remarque chez un homme...")
    st.subheader("")
       
    
def get_a_date():
    
    st.subheader("- Attractivité")
    attr = st.select_slider("1- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    st.subheader("- Sincérité")
    sinc = st.select_slider("2- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    st.subheader("- Intelligence")
    intel = st.select_slider("3- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    st.subheader("- Fun")
    fun = st.select_slider("4- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    st.subheader("- Ambition")
    amb = st.select_slider("5- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    st.subheader("- Passions Communes")
    shar = st.select_slider("6- Choisissez un score", options=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    
    X = FeatureSelector().women_decisions_features
    Y = FeatureSelector().women_decisions_target
    test_size = stg.TEST_SIZE
    seed = stg.SEED
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size, random_state=seed)
    get_date = Speed_Dating_Prediction(X, Y, X_train, X_test, Y_train, Y_test).single_prediction(attractivity=attr,
                                                                                                 sincerity=sinc,
                                                                                                 intelligence=intel,
                                                                                                 fun=fun, 
                                                                                                 ambition=amb, 
                                                                                                 shared_interests=shar)
    
    if attr != 0.0 and sinc != 0.0 and intel != 0.0 and fun != 0.0 and amb != 0.0 and shar != 0.0:
        st.write(get_date)
        if get_date == 1:
            st.success("J'aimerai bien te revoir...")
            st.image("https://37.media.tumblr.com/9a9f902a949d0adc4c9ae11a657fec07/tumblr_mmutrfJQIt1r1uwgno1_500.gif",
                     width=700,
                     caption=" ")
        else: 
            st.error("Je crois qu'on va en rester là...")
            st.image("https://media.giphy.com/media/mA1D0xsYF1fkuldOlP/giphy.gif",
                     width=700,
                     caption=" ")  
    
if __name__ == "__main__":
    project_description()
    get_a_date()
