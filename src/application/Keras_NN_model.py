import os
from os import path
from os.path import join
import sys

import src.settings.base as stg
from src.infrastructure.dataframecreation import DataFrameBuilder
from src.domain.featureselection import FeatureSelector

from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import recall_score

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import SGD

class Keras_classification:
    
    def __init__(self, X, Y, X_train, X_test, Y_train, Y_test):
        
        # Chargement des données, features et target
        self.X = FeatureSelector().women_decisions_features
        self.Y = FeatureSelector().women_decisions_target
        
        # Train - Test split
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X, self.Y, test_size=stg.TEST_SIZE, random_state=stg.SEED)
    
    
    def Keras_classifier(self,
                         seed = stg.SEED, 
                         test_size = stg.TEST_SIZE, 
                         num_folds = stg.NUM_FOLDS,
                         scoring = stg.SCORING, 
                         name = stg.NAME):
        
        # Keras-Scikit
        ker = KerasClassifier(build_fn=self.create_model, epochs=10, batch_size=10, verbose=1)
        
        ker.fit(self.X_train, self.Y_train)
        predictions = ker.predict(self.X_test)
        rec = recall_score(self.Y_test, predictions)
        
        return rec
    
    
    def create_model(self, neurons=12, activation='relu', learn_rate = 0.01, momentum=0):
        
        # create model
        model = Sequential()
        model.add(Dense(neurons, input_dim=self.X_train.shape[1], activation=activation))
        model.add(Dense(2, activation=activation))
        model.add(Dense(1, activation='sigmoid'))
        
        # Compile model
        optimizer = SGD(lr=learn_rate, momentum=momentum)
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        
        return model    
 
        
        
    


        
        