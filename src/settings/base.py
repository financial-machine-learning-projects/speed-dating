"""Basic settings of the project.
Contains all configurations for the project itself.
Should NOT contain any secrets.
>>> import settings
>>> settings.DATA_DIR
"""
import os
from os import path
from os.path import join
import sys

# Utils
TEST_SIZE = 0.2
SEED = 3
NUM_FOLDS = 10
SCORING = 'recall'
NAME = 'Gaussian Naive Bayes'

# Paths
REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))
DATA_DIR = os.path.join(REPO_DIR, "data")


# Data
SD_data = 'Speed-Dating-Data.csv'

# DataFrame columns
IID = 'iid'
GENDER = 'gender'
FIELD = 'field_cd'
INCOME = 'income'

AGE = 'age'
RACE = "race"
ATTRACTIVENESS = "attr"
SINCERITY = "sinc"
INTELLIGENCE = "intel"
FUN = 'fun'
AMBITION = 'amb'
SHARED_INTERESTS= "shar"

AGE_O = 'age_o'
RACE_O = "race_o"
ATTRACTIVENESS_O = "attr_o"
SINCERITY_O = "sinc_o"
INTELLIGENCE_O = "intel_o"
FUN_O = 'fun_o'
AMBITION_O = 'amb_o'
SHARED_INTERESTS_O = "shar_o"

SAMERACE = 'samerace'
INTEREST_CORRELATION = 'int_corr'
DECISION = "dec"
DECISION_O = 'dec_o'
MATCH = 'match'

COLS = [ATTRACTIVENESS_O, SINCERITY_O, 
        INTELLIGENCE_O, FUN_O, AMBITION_O, SHARED_INTERESTS_O]

TARGET = [DECISION_O]