import os
from os import path
from os.path import join
import sys

import pandas as pd

import src.settings.base as stg
from src.infrastructure.dataframecreation import DataFrameBuilder

class FeatureSelector:
    
    """ Select the appropriate features for the model. 

    Attributes
    ----------
    
    Methods
    -------

    """
    @property
    def features_data_large(self):
            
        df = DataFrameBuilder(stg.SD_data).data
        df = df[[stg.IID,
                 stg.GENDER,
                 stg.FIELD, 
                 stg.INCOME,
                 stg.AGE, stg.AGE_O, 
                 stg.RACE, stg.RACE_O, stg.SAMERACE,
                 stg.ATTRACTIVENESS, stg.ATTRACTIVENESS_O, 
                 stg.SINCERITY, stg.SINCERITY_O, 
                 stg.INTELLIGENCE, stg.INTELLIGENCE_O, 
                 stg.FUN, stg.FUN_O, 
                 stg.AMBITION, stg.AMBITION_O, 
                 stg.SHARED_INTERESTS, stg.SHARED_INTERESTS_O, stg.INTEREST_CORRELATION,
                 stg.DECISION, stg.DECISION_O, stg.MATCH]]
        
        return df
    
    @property
    def women_decisions(self):
        """Return women's point of view in a dataframe.
        
        Returns
        -------
        features: pandas.DataFrame
        """
            
        df = self.features_data_large.copy()
        df = df[df[stg.GENDER] != 0]
        df = df.drop([stg.IID,
                      stg.GENDER, 
                      stg.FIELD, stg.INCOME,
                      stg.AGE, stg.RACE,
                      stg.AGE_O, stg.RACE_O,
                      stg.ATTRACTIVENESS, stg.SINCERITY, stg.INTELLIGENCE, stg.FUN, stg.AMBITION, stg.SHARED_INTERESTS,
                      stg.SAMERACE, stg.INTEREST_CORRELATION, 
                      stg.DECISION, stg.MATCH], axis = 1)
        df_clean = df.dropna().reset_index(drop=True)
        
        return df_clean
    
    @property
    def women_decisions_features(self):
        """Return features dataframe from files with no target.
        
        Returns
        -------
        features: pandas.DataFrame
        """
        dataset_df = self.women_decisions.copy()
        if stg.DECISION_O in dataset_df.columns:
            features = dataset_df.drop(columns=[stg.DECISION_O])
            return features
        else:
            features = dataset_df
            return features

    @property
    def women_decisions_target(self):
        """Return target column from file (if exists in file).
        
        Returns
        -------
        target: pandas.core.series.Series
        """
        dataset_df = self.women_decisions.copy()
        if stg.DECISION_O in dataset_df.columns:
            target = dataset_df[stg.DECISION_O]
            return target
        else:
            return None